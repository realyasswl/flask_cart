import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from datetime import datetime
from flask_login import UserMixin
from cart import db


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)

    # User authentication information
    username = db.Column(db.String(50), nullable=False, unique=True)
    password = db.Column(db.String(255), nullable=False, server_default='')
    reset_password_token = db.Column(db.String(100), nullable=False, server_default='')

    # User email information
    email = db.Column(db.String(255), nullable=False, unique=True)
    registered_on = db.Column(db.DateTime())
    confirmed_at = db.Column(db.DateTime())

    # User information
    active = db.Column('is_active', db.Boolean(), nullable=False, server_default='0')
    first_name = db.Column(db.String(100), nullable=False, server_default='')
    last_name = db.Column(db.String(100), nullable=False, server_default='')

    # User balance
    balance = db.Column(db.Float)
    # Relationships
    roles = db.relationship('Role', secondary='user_role',
                            backref=db.backref('users', lazy='dynamic'))

    def __init__(self, username, password, email):
        self.username = username
        self.password = password
        self.email = email
        self.registered_on = datetime.utcnow()
        self.balance = 10000


class Role(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(50), unique=True)

    def __init__(self, name):
        self.name = name


class UserRole(db.Model):
    id = db.Column(db.Integer(), primary_key=True)
    user_id = db.Column(db.Integer(), db.ForeignKey('user.id', ondelete='CASCADE'))
    user = db.relationship('User',
                           backref=db.backref('userroles', lazy='dynamic'))
    role_id = db.Column(db.Integer(), db.ForeignKey('role.id', ondelete='CASCADE'))
    role = db.relationship('Role',
                           backref=db.backref('userroles', lazy='dynamic'))

    def __init__(self, user, role):
        self.user = user
        self.role = role


class Product(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)
    description = db.Column(db.String(50000))
    small_picture = db.Column(db.String(256))
    price = db.Column(db.Float)

    category_id = db.Column(db.Integer, db.ForeignKey('category.id'))
    category = db.relationship('Category',
                               backref=db.backref('products', lazy='dynamic'))

    def __init__(self, name, price, cat):
        self.name = name
        self.category = cat
        self.price = price

    def __repr__(self):
        return '<Product %r>' % self.name


class Category(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(256), nullable=False)

    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return '<Category %r>' % self.name


class Cart(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User',
                           backref=db.backref('carts', lazy='dynamic'))
    active = db.Column(db.Boolean)

    def __init__(self, user_id):
        self.user_id = user_id
        self.active = True


class CartProduct(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    product = db.relationship('Product',
                              backref=db.backref('cartProducts', lazy='dynamic'))
    count = db.Column(db.Integer)
    cart_id = db.Column(db.Integer, db.ForeignKey('cart.id'))
    cart = db.relationship('Cart',
                           backref=db.backref('cartProducts', lazy='dynamic'))

    def __init__(self, product_id, cart_id, count):
        self.product_id = product_id
        self.cart_id = cart_id
        self.count = count


class PurchaseDetail(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    purchase_id = db.Column(db.Integer, db.ForeignKey('purchase.id'))
    purchase = db.relationship('Purchase',
                               backref=db.backref('purchaseDetails', lazy='dynamic'))
    product_id = db.Column(db.Integer, db.ForeignKey('product.id'))
    product = db.relationship('Product',
                              backref=db.backref('purchaseDetails', lazy='dynamic'))
    count = db.Column(db.Integer)

    def __init__(self, purchase, product, count):
        self.product = product
        self.purchase = purchase
        self.count = count


class Purchase(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    user = db.relationship('User',
                           backref=db.backref('purchases', lazy='dynamic'))

    def __init__(self, user):
        self.user = user


if __name__ == '__main__':
    db.drop_all()
    db.create_all()
    admin = User(username="koei", password="cc90b5d1a2911178b52ceb63f2fd77fd", email="realyasswl@gmail.com")
    db.session.add(admin)
    user1 = User(username="user1", password="cc90b5d1a2911178b52ceb63f2fd77fd", email="user1@gmail.com")
    db.session.add(user1)
    laptop = Category(name="laptop")
    db.session.add(laptop)
    food = Category(name="food")
    db.session.add(food)
    drink = Category(name="drink")
    db.session.add(drink)

    thinkpad = Product(name="thinkpad", price=4000, cat=laptop)
    db.session.add(thinkpad)
    mac = Product(name="mac", price=5000, cat=laptop)
    db.session.add(mac)
    bread = Product(name="bread", price=8, cat=food)
    db.session.add(bread)
    beer = Product(name="beer", price=5, cat=drink)
    db.session.add(beer)

    pur = Purchase(user=user1)
    db.session.add(pur)

    pur_detail = PurchaseDetail(purchase=pur, product=thinkpad, count=2)
    db.session.add(pur_detail)
    pur_detail1 = PurchaseDetail(purchase=pur, product=mac, count=2)
    db.session.add(pur_detail1)

    role1 = Role(name="admin")
    db.session.add(role1)
    ur = UserRole(user=admin, role=role1)
    db.session.add(ur)
    db.session.commit()
