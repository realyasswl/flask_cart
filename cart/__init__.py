import flask_login
from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_principal import Principal


app = Flask(__name__, static_folder='../static', template_folder='../templates')

login_manager = flask_login.LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'login'

app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../s3.db'
app.config.setdefault('SQLALCHEMY_TRACK_MODIFICATIONS', True)
app.config['SECRET_KEY'] = "askldjfaklshjdklasdhjfgkljashdklfjasldkfjlasdkjf"
db = SQLAlchemy(app)
prin=Principal(app)
