import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
import logging

logging.basicConfig(format='[%(asctime)s] %(lineno)d: %(message)s', datefmt='%m/%d/%Y %I:%M:%S %p')

import hashlib
import pickle
import json
import math
import sqlalchemy
from flask_principal import Principal, Permission, RoleNeed, identity_loaded, UserNeed, Identity, identity_changed, \
    PermissionDenied
from flask_login import login_user, logout_user, current_user, login_required
import flask_user

from flask import render_template, redirect, url_for, request, flash, jsonify, render_template_string, session, \
    current_app
from cart.models import User, Category, Cart, CartProduct, Product, Purchase, PurchaseDetail
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.wsgi import WSGIContainer

from cart import login_manager, app, prin, db
from cart.forms import LoginForm, RegisterForm, AddToCartForm

admin_permission = Permission(RoleNeed('admin'))


@identity_loaded.connect_via(app)
def on_identity_loaded(sender, identity):
    # Set the identity user object
    identity.user = current_user

    # Add the UserNeed to the identity
    if hasattr(current_user, 'id'):
        identity.provides.add(UserNeed(current_user.id))

    # Assuming the User model has a list of roles, update the
    # identity with the roles that the user provides
    if hasattr(current_user, 'roles'):
        for role in current_user.roles:
            identity.provides.add(RoleNeed(role.name))


@app.route('/')
def hello_world():
    return render_template('home.html')


@login_manager.user_loader
def load_user(user_id):
    user = User.query.get(user_id)
    if user is None:
        print("user %s is None" % user_id)
        return None
    else:
        print("user %s is " % user_id, user)
        return user


@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dashboard.html')


@app.route("/product/<pid>")
@app.route("/product/", defaults={"pid": ""})
@app.route("/product", defaults={"pid": ""})
@login_required
def product(pid):
    if pid:
        pro = Product.query.filter_by(id=pid).first()
        return render_template('product.html', pro=pro)
    else:
        categories = Category.query.all()
        return render_template('product_list.html', categories=categories)


@app.route("/category/<cid>")
@app.route("/category/", defaults={"cid": ""})
@app.route("/category", defaults={"cid": ""})
@login_required
def category(cid):
    if cid:
        cate = Category.query.filter_by(id=cid).first()
        return render_template('category.html', cate=cate)
    else:
        categories = Category.query.all()
        return render_template('product_list.html', categories=categories)


@app.route('/admin')
@login_required
@admin_permission.require()
def admin():
    return render_template('admin.html')


@app.route('/register', methods=['GET', 'POST'])
def register():
    form = RegisterForm()
    if request.method == 'GET':
        return render_template('register.html', form=form)
    else:
        if form.validate_on_submit():
            u = User(username=form.username.data, password=hashlib.md5(form.password.data.encode("utf-8")).hexdigest(),
                     email=form.email.data)
            db.session.add(u)
            try:
                db.session.commit()
            except sqlalchemy.exc.IntegrityError as ex:
                flash(uniqueColumn("{0}".format(ex)).strip() + " already exists.")
                return render_template('register.html', form=form)
            return redirect(url_for('login'))
        else:
            flash_errors(form)
            return render_template('register.html', form=form)


@app.route('/add2cart', methods=['POST'])
def add2cart():
    form = AddToCartForm()
    pid = form.product_id.data
    count = form.count.data
    add_product(pid, count)
    return cartJson()


@app.route('/cart', methods=['GET'])
def cartJson():
    cart = current_cart()
    if cart:
        return list2json(cart.cartProducts, "product")
    return "{}"


def convert_to_builtin_type(b):
    d = {}
    d.update({your_key: b.__dict__[your_key] for your_key in {k for k in b.__dict__ if not k.find("_") == 0}})
    return d


def obj2json(obj, *subobj):
    d = convert_to_builtin_type(obj)
    for sub in subobj:
        d.update({sub: convert_to_builtin_type(getattr(obj, sub))})
    j = json.dumps(d)
    return j


@app.route("/purchase/<pid>")
@app.route("/purchase/", defaults={"pid": ""}, methods=["POST", "GET"])
@app.route("/purchase", defaults={"pid": ""}, methods=["POST", "GET"])
@login_required
def purchase(pid):
    if request.method == 'GET':
        if pid:
            return "purchase information with specific pid %s" % pid
        else:
            return "purchase list"
    else:
        cart = current_cart()
        pur = Purchase(user=current_user)
        db.session.add(pur)

        cost = 0

        for cp in cart.cartProducts:
            cost = cost + cp.count * cp.product.price
        if cost < current_user.balance:
            print(cost, current_user.balance)
            for cp in cart.cartProducts:
                pur_detail = PurchaseDetail(purchase=pur, product=cp.product, count=cp.count)
                db.session.add(pur_detail)
            current_user.balance = current_user.balance - cost
            cart.active = False
            db.session.commit()
            return '{"msg":"add to purchase","content":%s}' % cartJson()
        else:
            return '{"msg":"no sufficient fund","content":%s}' % cartJson()


def list2json(l, *subobj):
    m = map(lambda x: x, [obj2json(x, "product") for x in l])
    return "[%s]" % ",".join(list(m))


def add_product(pid, count):
    cart = current_cart()
    if not cart:
        cart = Cart(current_user.id)
        db.session.add(cart)
        db.session.commit()
    cp = CartProduct.query.filter_by(product_id=pid, cart_id=cart.id).first()
    if cp:
        cp.count += count
        # db.session.update(cp)
    else:
        cp = CartProduct(product_id=pid, cart_id=cart.id, count=count)
        db.session.add(cp)
    db.session.commit()


def current_cart():
    return Cart.query.filter_by(user_id=current_user.id, active=True).first()


@app.route('/logout')
def logout():
    logout_user()
    return render_template('logout.html')


@app.route('/login', methods=['GET', 'POST'])
def login():
    # print(current_user)
    if current_user.is_authenticated:
        return redirect(request.args.get('next') or '/')
    # Here we use a class of some kind to represent and validate our
    # client-side form data. For example, WTForms is a library that will
    # handle this for us, and we use a custom LoginForm to validate.
    form = LoginForm()
    if request.method == 'GET':
        return render_template('login.html', form=form)
    else:  # POST
        if form.validate_on_submit():
            # Login and validate the user.
            # user should be an instance of your `User` class
            u, message = form2user(form)
            if message is None:
                login_user(u)
                print(session["_id"])
                identity_changed.send(current_app._get_current_object(),
                                      identity=Identity(u.id))

                return redirect(request.args.get('next') or '/')
            else:
                flash(message=message)
                return render_template('login.html', form=form)
        else:  # error in form
            flash_errors(form)
            return render_template('login.html', form=form)


def form2user(form):
    username = form.username.data
    password = form.password.data
    md5ed = hashlib.md5(password.encode("utf-8")).hexdigest()
    # print(password, md5ed)
    user = User.query.filter_by(username=username).first()

    if user is None:
        print('User %s not found.' % username)
        return None, 'User %s not found.' % username
    else:
        if user.password == md5ed:
            return user, None
        else:
            return None, "password not match"


def flash_errors(form):
    for field, errors in form.errors.items():
        for error in errors:
            flash(u"Error in the %s field - %s" % (
                getattr(form, field).label.text,
                error
            ))


@app.errorhandler(PermissionDenied)
def handle_invalid_usage(error):
    return decorated("Permission Denied")


def decorated(s):
    return render_template_string(
        '''{%% extends "base.html" %%}{%% block content %%}<h1>%s</h1>{%% endblock %%}''' % s)


import re


def uniqueColumn(ex):
    m = re.search("(?<=failed:).*(?=\\[SQL)", ex)
    return m.group(0)


if __name__ == '__main__':
    http_server = HTTPServer(WSGIContainer(app))
    http_server.listen(50001)
    IOLoop.instance().start()
