from flask_wtf import Form
from wtforms import StringField, BooleanField, PasswordField, validators, IntegerField, FieldList
from wtforms.validators import DataRequired


class LoginForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    remember_me = BooleanField('remember_me', default=False)


class RegisterForm(Form):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[
        validators.DataRequired(),
        validators.EqualTo('re_password', message='Passwords must match')
    ])
    re_password = PasswordField('re_password', validators=[DataRequired()])
    email = StringField('email',
                        validators=[DataRequired(), validators.Email()])


class AddToCartForm(Form):
    count = IntegerField("count", validators=[DataRequired()])
    product_id = StringField("product_id", validators=[DataRequired()])